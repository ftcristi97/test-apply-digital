# test-apply-digital - Felipe Trejo Cristi

## Getting started

Esta es la prueba hecha para el cargo de Fullstack software engineer.

Se llevaron a cabo todo lo pedido como base. Además, se agregó el gitlab ci para los linter y test del backend que se corren en cada merge request.

Para esto, ya existe una base de datos creada en mongodb.

## Add these files

Para poder funcionar tiene que agregar:

### test-apply

Agregar un archivo .env con lo siguiente:

- hackerNewsUrl=https://hn.algolia.com/api/v1/search_by_date?query=nodejs
- mongoDBUrl=mongodb+srv://news-test:Cgzvm283.@news.ytfzlvs.mongodb.net/news?retryWrites=true&w=majority

### test-apply-frontend

Agregar un archivo .env con lo siguiente:

- REACT_APP_API_BASE_URL=http://backend:8080/

## Cómo correr los programas?

Para poder correr los programas porfavor correr el siguiente comando:

docker-compose up

Con este comando se correra el backend en el puerto 8080 y el frontend en el puerto 3000.
