import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type HackerNewsDocument = HydratedDocument<HackerNews>;

@Schema({ versionKey: false })
export class HackerNews {
  @Prop()
  object_id: string;

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  url: string;

  @Prop()
  story_url: string;

  @Prop()
  author: string;

  @Prop({ default: true })
  shouldDisplay: boolean;

  @Prop()
  created_at: Date;
}

export const HackerNewsSchema = SchemaFactory.createForClass(HackerNews);
