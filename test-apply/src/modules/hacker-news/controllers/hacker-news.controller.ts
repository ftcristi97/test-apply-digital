import { Controller, Get, Param, Put } from '@nestjs/common';
import { HackerNewsService } from '../services/hacker-news.service';
import { orderBy } from 'lodash';

@Controller('news')
export class HackerNewsController {
  constructor(private readonly appService: HackerNewsService) {}

  @Get()
  async getAll() {
    const allNews = await this.appService.getAll();
    return orderBy(allNews, ['created_at'], ['desc']);
  }

  @Put(':id')
  update(@Param('id') id: string) {
    return this.appService.blockNews({ objectId: id });
  }
}
