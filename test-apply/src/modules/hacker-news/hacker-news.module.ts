import { Module } from '@nestjs/common';
import { HackerNewsService } from 'src/modules/hacker-news/services/hacker-news.service';
import { HackerNewsController } from 'src/modules/hacker-news/controllers/hacker-news.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNews, HackerNewsSchema } from './schemas/hacker-news.schema';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: HackerNews.name, schema: HackerNewsSchema },
    ]),
  ],
  controllers: [HackerNewsController],
  providers: [HackerNewsService],
})
export class HackerNewsModule {}
