import { Test, TestingModule } from '@nestjs/testing';
import { HackerNewsController } from '../modules/hacker-news/controllers/hacker-news.controller';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, connect, Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { HackerNewsService } from '../modules/hacker-news/services/hacker-news.service';
import {
  HackerNews,
  HackerNewsDocument,
  HackerNewsSchema,
} from '../modules/hacker-news/schemas/hacker-news.schema';
import { HackerNewsFactory } from '../../test/factories/hacker-news.factory';
import { HttpService } from '@nestjs/axios';
import { createMock, DeepMocked } from '@golevelup/ts-jest';

describe('HackerNewsController', () => {
  let hackerNewsController: HackerNewsController;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let hackerNewsModel: Model<HackerNews>;
  let httpService: DeepMocked<HttpService>;
  let hackerNewsService: HackerNewsService;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    hackerNewsModel = mongoConnection.model(HackerNews.name, HackerNewsSchema);

    const app: TestingModule = await Test.createTestingModule({
      controllers: [HackerNewsController],
      providers: [
        HackerNewsService,
        { provide: getModelToken(HackerNews.name), useValue: hackerNewsModel },
        {
          provide: HttpService,
          useValue: createMock<HttpService>(),
        },
      ],
    }).compile();

    hackerNewsController = app.get<HackerNewsController>(HackerNewsController);
    httpService = app.get(HttpService);
    hackerNewsService = app.get(HackerNewsService);
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  afterEach(async () => {
    const collections = mongoConnection.collections;
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  });

  describe('get all news', () => {
    it('should return all news from database', async () => {
      //Arrange.
      const firstHackerNew = HackerNewsFactory.build({
        shouldDisplay: true,
        created_at: new Date('2023-02-02'),
      });
      const secondHackerNew = HackerNewsFactory.build({
        shouldDisplay: true,
        created_at: new Date('2023-02-01'),
      });
      await new hackerNewsModel(firstHackerNew).save();
      await new hackerNewsModel(secondHackerNew).save();

      //Act.
      const articles = await hackerNewsController.getAll();

      //Assert.
      const sanitizedArticles = articles.map((article) => ({
        object_id: article.object_id,
        title: article.title,
        story_title: article.story_title,
        url: article.url,
        story_url: article.story_url,
        author: article.author,
        shouldDisplay: article.shouldDisplay,
        created_at: article.created_at,
      }));
      expect(sanitizedArticles).toStrictEqual([
        firstHackerNew,
        secondHackerNew,
      ]);
    });
    it('should return empty if there are no news', async () => {
      //Arrange.
      //Act.
      const articles = await hackerNewsController.getAll();

      //Assert.
      expect(articles).toStrictEqual([]);
    });
    it('should test the scheduler', async () => {
      //Arrange.
      const expectedLinkResponse = {
        hits: [
          {
            objectID: '35747908',
            title: null,
            story_title: 'Why I develop on Windows',
            url: null,
            story_url:
              'https://blog.shortround.dev/why-i-think-windows-is-the-best-dev-platform/',
            author: 'Swalden123',
            shouldDisplay: true,
            created_at: '2023-04-28T22:27:43.000Z',
          },
          {
            objectID: '35746599',
            title: null,
            story_title: 'Is Gmail killing independent email?',
            url: null,
            story_url:
              'https://tutanota.com/blog/posts/gmail-independent-email',
            author: 'LinuxBender',
            shouldDisplay: true,
            created_at: '2023-04-28T20:21:06.000Z',
          },
        ],
      };

      const expectedResponse = {
        hits: [
          {
            object_id: '35747908',
            story_title: 'Why I develop on Windows',
            url: null,
            story_url:
              'https://blog.shortround.dev/why-i-think-windows-is-the-best-dev-platform/',
            author: 'Swalden123',
            shouldDisplay: true,
            created_at: new Date('2023-04-28T22:27:43.000Z'),
          },
          {
            object_id: '35746599',
            story_title: 'Is Gmail killing independent email?',
            url: null,
            story_url:
              'https://tutanota.com/blog/posts/gmail-independent-email',
            author: 'LinuxBender',
            shouldDisplay: true,
            created_at: new Date('2023-04-28T20:21:06.000Z'),
          },
        ],
      };

      httpService.axiosRef.mockResolvedValueOnce({
        data: expectedLinkResponse,
        headers: {},
        config: { url: '' },
        status: 200,
        statusText: '',
      });

      //Act.
      await hackerNewsService.importHackerNews();

      //Assert.
      const allHackerNews = await hackerNewsController.getAll();
      const sanitizedResponse = allHackerNews.map((news) => ({
        object_id: news.object_id,
        story_title: news.story_title,
        url: news.url,
        story_url: news.story_url,
        author: news.author,
        shouldDisplay: news.shouldDisplay,
        created_at: news.created_at,
      }));
      expect(sanitizedResponse).toStrictEqual(expectedResponse.hits);
    });
    it('should return all news from database', async () => {
      //Arrange.
      const firstHackerNew: HackerNewsDocument = HackerNewsFactory.build({
        shouldDisplay: true,
        created_at: new Date('2023-02-02'),
      });
      const secondHackerNew: HackerNewsDocument = HackerNewsFactory.build({
        shouldDisplay: true,
        created_at: new Date('2023-02-01'),
      });
      await new hackerNewsModel(firstHackerNew).save();
      await new hackerNewsModel(secondHackerNew).save();

      //Act.
      await hackerNewsController.update(firstHackerNew.object_id);

      //Assert.
      const news = await hackerNewsModel.findOne({
        object_id: firstHackerNew.object_id,
      });
      expect(news.shouldDisplay).toBeFalsy();
    });
  });
});
