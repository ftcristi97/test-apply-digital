/** @type {import('tailwindcss').Config} */
// eslint-disable-next-line no-undef
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'apply-title-color': '#333333',
        'apply-author-color': '#999999',
        'apply-border-color': '#CCCCCC',
      },
      fontSize: {
        xl: '13pt'
      }
    },
  },
  plugins: [],
}

