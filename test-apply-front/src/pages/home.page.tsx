import { useEffect, useState } from "react"
import { News } from "../types/news.type";
import { Row } from "../components/row";
import { client } from "../config/client";
import { parseDates } from "../helpers/parse-dates.helper";

export const Home = () => {
  const [news, setNews] = useState<News[]>([])

  const blockNews = async ({ id }: { id: string }) => {
    try{
      await client.put(`news/${id}`);
      const filteredNews = news.filter((news) => news.object_id !== id)
      setNews(filteredNews)
    } catch {
      alert('Hubo un problema eliminando la noticia')
    }
  }

  useEffect(() => {
    const getNews = async () => {
      try{
        const response = await client.get('news');
        const parsedData = parseDates({ news: response.data })
        setNews(parsedData);
      } catch {
        alert('Hubo un problema recopilando las noticias');
      }
    }

    getNews();
  }, [])
  return (
    <div>
      {news ? 
      news.map((news) => <Row blockNews={blockNews} key={news.object_id} news={news} />)
      :
      <div>no hay nada</div>}
    </div>
  )
}