import { News } from "../types/news.type"
import DeleteIcon from '@heroicons/react/20/solid/TrashIcon';

export const Row = ({ news, blockNews }: { news: News, blockNews: ({id}: {id: string}) => Promise<void> }) => {


  return (
    <div className="group flex flex-row items-center hover:bg-zinc-50 border-b border-apply-border-color mx-8 h-16">
      <a target='_blank' href={news.story_url || news.url} className="group-hover:bg-zinc-50 w-11/12 flex flex-row bg-white items-center" rel="noreferrer">
        <div className="flex pl-2 flex-row w-5/6">
          <p className="text-apply-title-color text-xl">{news.story_title ?? news.title}</p>
          <p className="pl-3 text-apply-author-color"> - {news.author} - </p>
        </div>
        <p className="flex justify-end text-apply-title-color text-xl">{news.created_at}</p>
      </a>
      <DeleteIcon className="h-6 w-1/12 invisible group-hover:visible" onClick={() => blockNews({ id: news.object_id})} />
    </div>
  )
}