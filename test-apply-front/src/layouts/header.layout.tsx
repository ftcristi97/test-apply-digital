export const Header = () => {
  return (
    <div className="h-52 bg-cyan-100 flex flex-col justify-center ">
      <div className="pl-7">
        <p className="text-2xl italic font-bold">Apply Digital Test</p>
        <p>We love hacker news!</p>
      </div>
    </div>
  )
}
