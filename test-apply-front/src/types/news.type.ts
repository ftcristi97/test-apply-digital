export type News = {
  object_id: string;
  title: string;
  story_title: string;
  url: string;
  story_url: string;
  author: string;
  shouldDisplay: boolean;
  created_at: string;
}