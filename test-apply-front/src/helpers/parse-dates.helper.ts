import { News } from "../types/news.type";

const MonthList = ['Jan', 'Feb', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

export const parseDates = ({ news }: { news: News[]}) => {
  return news.map((news) => {
    const parsedDate = new Date(news.created_at);
    const now = new Date();
    const yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    let parsedCreatedAt;
    if (parsedDate.getDate() === now.getDate() && parsedDate.getMonth() === now.getMonth() && parsedDate.getFullYear() === now.getFullYear()){
      const hours = parsedDate.getHours();
      const minutes = parsedDate.getMinutes();
      const hourPeriod = hours >= 12 ? 'pm' : 'am';
      parsedCreatedAt = `${hours % 12 ? hours % 12 : 12}:${minutes < 10 ? '0'+minutes : minutes} ${hourPeriod}`
    } else {
      if (parsedDate.getDate() === yesterday.getDate() && parsedDate.getMonth() === yesterday.getMonth() && parsedDate.getFullYear() === yesterday.getFullYear()) {
        parsedCreatedAt = 'yesterday';
      } else {
        const month = parsedDate.getMonth();
        const day = parsedDate.getDay();
        parsedCreatedAt = `${MonthList[month]} ${day}`;
      }
    }

    return {
      ...news,
      created_at: parsedCreatedAt,
    }
    
  })
}